h1. To Build:

xcodebuild -project ParallelBC.xcodeproject -target ParallelBC build

h1. To get info about other targets:

xcodebuild -list -project ParallelBC.xcodeproject


To run:

./ParallelBC input_filename output_filename

