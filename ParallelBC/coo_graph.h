/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   coo_graph.h
 * Author: daveshepard
 *
 * Created on June 8, 2016, 10:13 PM
 */

#ifndef COO_GRAPH_H
#define COO_GRAPH_H

#include <OpenCL/opencl.h>

typedef struct __attribute__ ((packed)) {
    cl_long node_count;
    cl_int *end_node_offsets;
    cl_int edge_count;
    cl_long *end_node_ids;
    cl_int *weights;
} coo_graph; 

long count_neighbors(coo_graph* graph, int start_vertex);

#endif /* COO_GRAPH_H */

