//
//  coo_builder.h
//  ParallelBC
//
//  Created by Dave Shepard on 6/10/16.
//  Copyright © 2016 Dave Shepard. All rights reserved.
//

#ifndef coo_builder_h
#define coo_builder_h


#include "BoostTypes.h"
coo_graph *coo_graph_from_GraphBuilder(GraphBuilder *builder, coo_graph *in_edges_graph);
coo_graph *load_graph_file(std::string filename, coo_graph *in_edges_graph);



#endif /* coo_builder_h */
