
/* 
 * File:   BoostTypes.h
 * Author: dave
 *
 * Created on June 9, 2016, 12:55 PM
 */

#ifndef BOOSTTYPES_H
#define BOOSTTYPES_H

#include <boost/graph/adjacency_list.hpp>

using namespace boost;

typedef property<edge_weight_t, double> EdgeWeightProperty;
typedef boost::adjacency_list<listS, vecS, boost::bidirectionalS, no_property, EdgeWeightProperty> GraphBuilder;
typedef boost::property_map<GraphBuilder, boost::edge_weight_t>::type WeightMap;

typedef graph_traits<GraphBuilder>::vertex_descriptor VertexDescriptor;
typedef GraphBuilder::edge_descriptor Edge;

#endif /* BOOSTTYPES_H */

