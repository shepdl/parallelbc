#include <boost/graph/adjacency_list.hpp>


#include <iostream>
#include "coo_graph.h"
#include "coo_builder.h"
#include "BoostTypes.h"
#include <iostream>
#include <fstream>

using namespace boost;


coo_graph *coo_graph_from_GraphBuilder(GraphBuilder *builder, coo_graph *in_edges_graph) {

    // Get node and edge counts
    unsigned long nodeCount = builder->m_vertices.size();
    int edgeCount = 0;
    graph_traits<GraphBuilder>::edge_iterator edgeCounterIterator, edgeCountIteratorEnd, nextEdgeCount;
    boost::tie(edgeCounterIterator, edgeCountIteratorEnd) = edges(*builder);
    for (nextEdgeCount = edgeCounterIterator; edgeCounterIterator != edgeCountIteratorEnd; edgeCounterIterator = nextEdgeCount) {
        ++nextEdgeCount;
        ++edgeCount;
    }

    int *end_node_offsets = (int *) malloc(sizeof(int) * (nodeCount + 1));
    cl_long *end_node_ids = (cl_long *) malloc(sizeof(cl_long) * edgeCount);
    int *weights = (int *) malloc(sizeof(int) * edgeCount);
    std::cout << "Node count: " << nodeCount << std::endl;
    printf("Edge count: %d\n", edgeCount);
    
    int *rev_end_node_offsets = (int *)malloc(sizeof(int) * (nodeCount + 1));
    cl_long *rev_end_node_ids = (cl_long *)malloc(sizeof(cl_long) * edgeCount);

    
    // Because the last node's stopping point is the end of the array,
    // which is equal to the total number of edges in the graph.
    end_node_offsets[nodeCount] = edgeCount;
    rev_end_node_offsets[nodeCount] = edgeCount;
    
//    std::cout << edgeCount << std::endl;
    
    for (int i = 0; i < nodeCount; i++) {
        end_node_offsets[i] = 0;
        rev_end_node_offsets[i] = 0;
    }
    
    for (int i = 0; i < nodeCount + 1; i++) {
//        std::cout << end_node_offsets[i] << std::endl;
//        std::cout << rev_end_node_offsets[i] << std::endl;
    }
    
//    std::cout << "Done" << std::endl;
    
    for (int i = 0; i < edgeCount; i++) {
        end_node_ids[i] = 0;
        weights[i] = 0;
        
        rev_end_node_ids[i] = 0;
    }


    // Build end_node_offsets array
    int total_edges_counter = 0;
    int in_edges_counter = 0;
    
    
    std::vector<VertexDescriptor> *nodeIds = new std::vector<VertexDescriptor>();
    WeightMap weightMap = boost::get(boost::edge_weight_t(), *builder);
    graph_traits<GraphBuilder>::vertex_iterator vertexIterator, vertexIteratorEnd;
    for (boost::tie(vertexIterator, vertexIteratorEnd) = vertices(*builder); vertexIterator != vertexIteratorEnd; ++vertexIterator) {
//    for (int i = 0; i < nodeCount; i++) {
//        nodeIds->push_back(vertexIterator);
        graph_traits<GraphBuilder>::vertex_descriptor thisVertex = vertex(*vertexIterator, *builder);
        nodeIds->push_back(thisVertex);
//        std::cout << thisVertex << std::endl;
        
        graph_traits<GraphBuilder>::out_edge_iterator edgeIterator, edgeIteratorEnd, nextEdge;
        boost::tie(edgeIterator, edgeIteratorEnd) = out_edges(thisVertex, *builder);
        end_node_offsets[*vertexIterator + 1] = total_edges_counter;
        for (nextEdge = edgeIterator; edgeIterator != edgeIteratorEnd; edgeIterator = nextEdge) {

            long targetId = target(*nextEdge, *builder);

            graph_traits<GraphBuilder>::edge_descriptor thisEdge = nextEdge.dereference();

            // Build end_node_ids array
            end_node_offsets[*vertexIterator + 1] += 1;
            end_node_ids[total_edges_counter] = targetId;
            
            // Build weights array
            weights[total_edges_counter] += get(weightMap, thisEdge);

            total_edges_counter += 1;
            ++nextEdge;
        }
        
        graph_traits<GraphBuilder>::in_edge_iterator inEdgeIterator, inEdgeIteratorEnd, inNextEdge;
        boost::tie(inEdgeIterator, inEdgeIteratorEnd) = in_edges(thisVertex, *builder);
        
        rev_end_node_offsets[*vertexIterator + 1] = in_edges_counter;
        
//        printf("IEC: %d\n", in_edges_counter);
        
        for (inNextEdge = inEdgeIterator; inEdgeIterator != inEdgeIteratorEnd; inEdgeIterator = inNextEdge) {
            
            long sourceId = source(*inNextEdge, *builder);
//            std::cout << "Predecessor of " << thisVertex << " is " << sourceId << std::endl;
            
//            std::cout << *vertexIterator << " :" << rev_end_node_offsets[*vertexIterator + 1] << std::endl;
            rev_end_node_offsets[*vertexIterator + 1] += 1;
//            std::cout << *vertexIterator << " :" <<  *vertexIterator << " node " << std::endl;
            
            rev_end_node_ids[in_edges_counter] = sourceId;
            
            in_edges_counter += 1;
            ++inNextEdge;
        }
        
    }
    
    for (int i = 0; i < nodeCount + 1; i++) {
//        std::cout << "Offset: " << rev_end_node_offsets[i] << std::endl;
//        std::cout << "Edge: " << rev_end_node_ids[i] << std::endl;
    }
    
    int max_weight = 0;
    for (int i = 0; i < edgeCount; i++) {
        if (weights[i] > max_weight) {
            max_weight = weights[i];
        }
    }
    
    max_weight += 1;
    
    for (int i = 0; i < edgeCount; i++) {
        weights[i] = max_weight - weights[i];
    }


    coo_graph * graph = (coo_graph *) malloc(sizeof(coo_graph));

    graph->node_count = nodeCount;
    graph->end_node_offsets = end_node_offsets;
    graph->edge_count = edgeCount;
    graph->end_node_ids = end_node_ids;
    graph->weights = weights;
    
    in_edges_graph->node_count = nodeCount;
    in_edges_graph->end_node_offsets = rev_end_node_offsets;
    in_edges_graph->end_node_ids = rev_end_node_ids;
    in_edges_graph->edge_count = edgeCount;

    return graph;
}


long count_neighbors(coo_graph *graph, int start_vertex) {
    
    long start_offset = graph->end_node_offsets[start_vertex];
    long end_offset;

    if (start_vertex < graph->node_count) {
        end_offset = graph->end_node_offsets[start_vertex + 1];
    } else {
        end_offset = graph->node_count;
    }

    return end_offset - start_offset;
}

coo_graph *load_graph_file(std::string filename, coo_graph *in_edges_graph) {

    GraphBuilder *builder = new GraphBuilder();
    
    // load file
    std::ifstream source_file(filename);
    std::string line;
    std::getline(source_file, line); // skip header
    while (std::getline(source_file, line)) {
        int sourceId, targetId, weight;
        char sep1, sep2;
        std::string date;
        std::istringstream iss(line);

        if (!(iss >> sourceId >> sep1 >> targetId >> sep2 >> weight)) break;
        VertexDescriptor source = vertex(sourceId, *builder);
        VertexDescriptor target = vertex(targetId, *builder);
        

        add_edge(source, target, weight, *builder);
//        }
    }

    std::cout << filename << " loaded" << std::endl;

    coo_graph *graph = coo_graph_from_GraphBuilder(builder, in_edges_graph);

    /*
    for (int i = 0; i < graph->edge_count; i++) {
        std::cout << "Edge " << graph->end_node_ids[i] << " has weight " << graph->weights[i] << std::endl;
    }
    */
    return graph;
}
