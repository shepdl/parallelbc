/**
 * Several arrays are 2-D arrays on the device, which
 * are preallocated because they are too large to fit in local memory.
 * Use get_offset to get the correct segment of the memory for this.
 * The first element is equivalent to global_id * num_users.
 *
 * Additionally, since in the BFS, each thread handles separate nodes,
 * each thread accesses only those elements of the memory that fall under
 * its purveiew. The step between these is equivalent to the total number of
 * threads. So, you start at thread position (local_id) and increase by
 * local id. So, loop position * thread_count (=local_size) + thread_id
 *
 * Therefore, to find the position for one thread in a loop in a device array,
 * calculate:
 *      (global_id * num_users) + (loop position * thread count) + thread_id
 *
 */

#include "ProblemConstants.h"



#define NUM_USERS 1299968
#define global_true_position get_global_id(0) * NUM_USERS
#define thread_position(loop_position) loop_position * get_local_size(0)+get_local_id(0)
#define global_true_position_from_loop_position(loop_position) get_global_id(0) * NUM_USERS + loop_position * get_local_size(0) + get_local_id(0)

#define DEBUG
#ifndef DEBUG
    #define printf // 
#endif


kernel void find_distance_for_node(
                                // global _st_coo_graph *graph,
                                // int node_index,
                                long node_count,
//                                constant int * start_index,
                                global long *end_node_ids,
                                global int *end_node_offsets,
                                global long *rev_end_node_ids,
                                global int *rev_end_node_offsets,
                                int edge_count,
                                global int *weights,
                                   // The output! Indexed by the node
//                                global int *total_shortest_paths_a_node_is_on,
                                   
                                   // these two are the "Work" arrays, which are also shared
                                global int *bfs_distances,
                                global int *shortest_paths_from_the_root_to_others,
                                global float *dependencies,
                                global float *betweenness_centrality_measures
//                                global int *bfs_queue,
//                                global int *bfs_queue_2
                                ) {
    
//    printf("Kernel started\n");
    
    
    // TODO: don't we need some kind of modulus on NUM_BLOCKS to limit the size of bfs_distances?
//    long node_count = 1300000 - (1300000 % 512);    
    
//    for (int i = 0; i < node_count; i++) {
//        printf("Node %d starts at %d\n", i, end_node_offsets[i]);
//    }
//

    int num_blocks = NUM_BLOCKS;
    
    size_t node_id = get_global_id(0);
    int buffer_row = node_id % num_blocks;
    int buffer_row_start = buffer_row * node_count;
    int buffer_row_end = buffer_row_start + node_count;
    
    int edge_buffer_row = node_id % num_blocks;
    int edge_buffer_row_start = edge_buffer_row * edge_count;
    int edge_buffer_row_end = edge_buffer_row_start + edge_count;
    
    
//    printf("[Kernel %lu] Buffer from %d to %d\n", node_id, buffer_row_start, buffer_row_end);
    
    size_t thread_count = get_local_size(0);
    size_t thread_id = get_local_id(0);
    
    int queue_size = 500; // This may look silly, but you can't allocate memory dynamically on the device
    local long bfs_queue[500];
    local long bfs_queue_2[500];
    
    for (int i = 0; i < edge_count; i++) {
        rev_end_node_ids[i] = -1;
    }
    
    
    
//    printf("Initialized: block %lu, thread %lu working on node %lu \n", node_id, thread_id, node_id);

    for (long k = buffer_row_start + thread_id; k < buffer_row_end; k += thread_count) {
        if (k - buffer_row_start == node_id) {
            bfs_distances[k] = 0;
            shortest_paths_from_the_root_to_others[k] = 1;
        } else {
            bfs_distances[k] = INT_MAX;
            shortest_paths_from_the_root_to_others[k] = 0;
        }
    }
    
    
    local int Q_len;
    local int Q2_len;
    
    if (thread_id == 0) {
        Q_len = 1;
        Q2_len = 0;
        bfs_queue[0] = node_id;
//        printf("Node ID here (%d) and in bfs_queue (%d)\n", node_id, bfs_queue[0]);
    }
    
    printf("Distance %d: %d\n", node_id, bfs_distances[0]);
    
    
    barrier(CLK_LOCAL_MEM_FENCE);
    
    int max_distance = 0;
    
    while (1) {
//        printf("[Kernel %lu Thread %lu] Loop top\n", node_id, thread_id);
        
        for (int k = thread_id; k < Q_len; k += thread_count) {
            long source_vertex = bfs_queue[k];
            printf("[Kernel %lu Thread %lu] Loaded %d from BFS queue\n", node_id, thread_id, source_vertex);
            long actual_source_vertex_pos = buffer_row_start + source_vertex;
            
//            printf("[Kernel %lu Thread %lu] Checking vertices from %d to %d\n", node_id, thread_id, end_node_offsets[source_vertex], end_node_offsets[source_vertex + 1]);
            for (
                 int target_vertex_counter = end_node_offsets[source_vertex];
                 target_vertex_counter < end_node_offsets[source_vertex + 1];
                 target_vertex_counter++
                 ) {
                
                printf("[Kernel %lu Thread %lu] Getting target vertex for counter %d\n", node_id, thread_id, target_vertex_counter);
                long target_vertex = end_node_ids[target_vertex_counter];
//                                printf("Getting target vertex offset for %ld and %ld\n", buffer_row_start, target_vertex);
                long actual_target_vertex_pos = buffer_row_start + target_vertex;
                printf("[Kernel %lu Thread %lu] Checking vertex from (%d/%ld) to (%d/%ld)\n", node_id, thread_id, source_vertex, actual_source_vertex_pos, target_vertex, actual_target_vertex_pos);
                int edge_weight = weights[target_vertex_counter];
                
//                printf("[Kernel %lu Thread %lu] Comparing %lu and %lu\n", source_vertex, target_vertex);
                printf("[Kernel %lu Thread %lu] Finding minimum of bfs_distances (%d/%ld and %d/%ld)\n", node_id, thread_id, source_vertex, actual_source_vertex_pos, target_vertex, actual_target_vertex_pos);
                // TODO: figure out how to handle INT_MAX here--if we're checking that the new
                
                int old_target_distance = bfs_distances[actual_target_vertex_pos];
                if (atomic_min(&bfs_distances[actual_target_vertex_pos], bfs_distances[actual_source_vertex_pos] + edge_weight) < old_target_distance || old_target_distance == INT_MAX) {
//                if (atomic_cmpxchg(&bfs_distances[actual_target_vertex_pos], INT_MAX, bfs_distances[actual_source_vertex_pos] + edge_weight) == INT_MAX) {
                    if (Q2_len >= queue_size) {
                        printf("[Kernel %lu Thread %lu] Exhausted queue\n", node_id, thread_id);
                        break;
                    }
                    if (bfs_distances[actual_source_vertex_pos] + edge_weight > max_distance) {
                        max_distance = bfs_distances[actual_source_vertex_pos] + edge_weight;
                    }
                    printf("[Kernel %lu Thread %lu] Add %d to queue\n", node_id, thread_id, target_vertex);
                    // int t = atomic_add(&Q2_len, 1);
                    int t = atom_inc(&Q2_len);
                    shortest_paths_from_the_root_to_others[target_vertex] = 0;
                    bfs_queue_2[t] = target_vertex;
                    
                    // Zero out predecessor list (effectively empty predecessors)
                    printf("Zeroing out predecessor list from %d to %d\n", edge_buffer_row_start + rev_end_node_offsets[target_vertex], edge_buffer_row_start + rev_end_node_offsets[target_vertex + 1]);
                    for (int i = edge_buffer_row_start + rev_end_node_offsets[target_vertex]; i < edge_buffer_row_start + rev_end_node_offsets[target_vertex + 1]; i++) {
                        rev_end_node_ids[i] = -1;
                    }
                }
                printf("[Kernel %lu Thread %lu] Comparison of source (%d/%ld) and target (%d/%ld) distances to update shortest paths \n", node_id, thread_id, source_vertex, actual_source_vertex_pos, target_vertex, actual_target_vertex_pos);
                if (bfs_distances[actual_target_vertex_pos] <= bfs_distances[actual_source_vertex_pos] + edge_weight) {
                    printf("[Kernel %lu Thread %lu] Updated shortest path count for %d ...\n", node_id, thread_id, target_vertex);
//                    bfs_distances[actual_source_vertex_pos] = bfs_distances[actual_target_vertex_pos] + edge_weight;
                    atomic_add(
                        &shortest_paths_from_the_root_to_others[actual_target_vertex_pos],
                        shortest_paths_from_the_root_to_others[actual_source_vertex_pos] //* edge_weight
                    );
                    for (int i = edge_buffer_row_start + rev_end_node_offsets[target_vertex]; i < edge_buffer_row_start + rev_end_node_offsets[target_vertex + 1]; i++) {
                        if (rev_end_node_ids[i] == -1) {
                            printf("Found space for predecessor\n");
                            rev_end_node_ids[i] = source_vertex;
                            break;
                        }
                    }
                } else {
                    // TODO: remove from rev_end_node_ids in order to set predecessors
                }
            }
        }
        
        
        barrier(CLK_LOCAL_MEM_FENCE);
        
        if (Q2_len == 0) {
            
            break;
        } else {
            
            for (int k = thread_id; k < queue_size; k += thread_count) {
                bfs_queue[k] = bfs_queue_2[k];
                bfs_queue_2[k] = 0;
            }
            
            
            barrier(CLK_LOCAL_MEM_FENCE);
            
            if (thread_id == 0) {
                Q_len = Q2_len;
                Q2_len = 0;
            }
            
            barrier(CLK_LOCAL_MEM_FENCE);
        }
    }

    // Now, go through each node and calculate its betweenness by calculating the dependencies of all the nodes, starting from the furthest out
    
    // If we create an inverted graph when building the graph, also in COO format, and then start from the nodes
    // in Q, then we can go backwards and process the nodes in reverse order of dependency.
    
    for (int i = thread_id; i < node_count; i++) {
        dependencies[buffer_row_start + i] = 0.0f;
    }
    
    /*
    for (int i = 0; i < node_count + 1; i++) {
        printf("Node: %d\n", rev_end_node_offsets[i]);
    }
    */
    barrier(CLK_LOCAL_MEM_FENCE);
    
    printf("Max distance: %d\n", max_distance);
    
    for (int i = edge_buffer_row_start; i < edge_buffer_row_end; i++) {
//        printf("%d\n", rev_end_node_ids[i]);
    }
    

    
    for (int distance = max_distance; distance > 0; distance--) {
        for (int current_id = thread_id; current_id < node_count; current_id += thread_count) {
            
            long actual_current_vertex_pos = buffer_row_start + current_id;
            
            if (bfs_distances[actual_current_vertex_pos] == distance) {
                printf("[Kernel %d] Node %d is at distance of %d\n", node_id, current_id, bfs_distances[actual_current_vertex_pos]);
                for (
                     int predecessor_counter = rev_end_node_offsets[current_id];
                     predecessor_counter < rev_end_node_offsets[current_id + 1];
                     predecessor_counter++
                     ) {
                    long predecessor_id = rev_end_node_ids[edge_buffer_row_start + predecessor_counter];
                    
//                    printf("Predecessor: %d\n", predecessor_id);
                    
                    if (predecessor_id == -1) {
                        break;
                    }
                    long actual_predecessor_pos = buffer_row_start + predecessor_id;
                    
                    printf("[Kernel %d] Checking %d against %d, which is at distance of %d \n", node_id, current_id, predecessor_id, bfs_distances[actual_predecessor_pos]);
                    
                    
                    // then calculate the depedency of current_id
                    // I think these need to be actual positions rather than node_ids
                    //                atomic_add(&dependencies[predecessor_id], (
                    
                    int edge_weight = 0;
                    for (int j = end_node_offsets[predecessor_id]; j < end_node_offsets[predecessor_id + 1]; j++) {
                        if (end_node_ids[j] == current_id) {
                            edge_weight = end_node_ids[j];
                            break;
                        }
                    }
                    if (edge_weight == 0) {
                        printf("Failed to find weight\n");
                    }
                    printf("[Kernel %d], SPP: %d  SPC: %d DC: %f  DP: %f\n", node_id, shortest_paths_from_the_root_to_others[actual_predecessor_pos], shortest_paths_from_the_root_to_others[actual_current_vertex_pos], dependencies[actual_current_vertex_pos], dependencies[actual_predecessor_pos]);
                    float depedency = (
                                       (float)shortest_paths_from_the_root_to_others[actual_predecessor_pos] /
                                       (float)shortest_paths_from_the_root_to_others[actual_current_vertex_pos]
                                       ) * (1.0f + dependencies[actual_current_vertex_pos])
                    ;
                    
                    printf("[Kernel %d] Dependency for %d (at %d): %f\n", node_id, predecessor_id, actual_predecessor_pos, depedency);
                    dependencies[actual_predecessor_pos] += depedency;
                }
                
                // Then set centrality, per http://algo.uni-konstanz.de/publications/b-fabc-01.pdf (p.10)
                if (current_id != node_id) {
                    printf("[Kernel %d] BC for %d: %f\n", node_id, current_id, betweenness_centrality_measures[actual_current_vertex_pos]);
                    betweenness_centrality_measures[actual_current_vertex_pos] += dependencies[actual_current_vertex_pos];
                }
            }
        }
        
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    
}

