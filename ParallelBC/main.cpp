/*
 * File:   main.cpp
 * Author: daveshepard
 *
 * Created on June 8, 2016, 7:56 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

#include <OpenCL/opencl.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/dynamic_bitset/dynamic_bitset.hpp>

//#include <boost/graph/use_mpi.hpp>
//#include <boost/graph/distributed/mpi_process_group.hpp>
//#include <boost/graph/distributed/betweenness_centrality.hpp>
//#include <boost/graph/betweenness_centrality.hpp>

#include "coo_graph.h"
#include "coo_builder.h"
#include "ProblemConstants.h"


#include "betweenness.cl.h"


using namespace std;
using namespace boost;



/*
 *
 */
int main(int argc, char** argv) {
    
    std::string input_filename,
         output_filename
    ;
    
    if (argc < 3) {
        input_filename = std::string("/Users/daveshepard/Documents/Research and Projects/Jishin/SISA/parallelbc/test-graph.csv.keep");
        output_filename = std::string("/Users/daveshepard/Documents/Research and Projects/Jishin/SISA/parallelbc/test-graph.csv.out");
//        printf("Usage: %s [input file] [output file]\n", argv[0]);
//        exit(0);
    } else {
        input_filename = argv[1];
        output_filename = argv[2];
    }
    
    std::cout << "Initializing program to read from " << input_filename << " ..." << std::endl;
//    coo_graph *graph = load_graph_file("/Users/dave/Documents/Projects/TwitterHarvester/betweenness/pbc-fixing/2011-03-12-w.csv");
    coo_graph *in_edges_graph = (coo_graph *)malloc(sizeof(coo_graph));
    coo_graph *graph = load_graph_file(input_filename, in_edges_graph);
    
//    for (int i = 0; i < in_edges_graph->edge_count; i++) {
//        std::cout << in_edges_graph->end_node_offsets[i] << std::endl;
//    }
    
    // copy graph to device
    cl_long d_node_count = graph->node_count;
//    std::cout << "Host: " << graph->node_count; // << " and device: " << *d_node_count << std::endl;
    cl_int d_edge_count = graph->edge_count;
    
    for (int i = 0; i < graph->node_count; i++) {
//        printf("Node %d starts at %d\n", i, graph->end_node_offsets[i]);
    }

    size_t work_group_size = NUM_BLOCKS;

    void *d_end_node_offsets = gcl_malloc((graph->node_count + 1) * sizeof(int), graph->end_node_offsets, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *d_end_node_ids = gcl_malloc(graph->edge_count * sizeof(long), graph->end_node_ids, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *d_weights = gcl_malloc(graph->edge_count * sizeof(int), graph->weights, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    
    void *d_rev_end_node_offsets = gcl_malloc((graph->node_count + 1) * sizeof(int), in_edges_graph->end_node_offsets, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
//    void *d_rev_end_node_ids = gcl_malloc(graph->edge_count * sizeof(long), in_edges_graph->end_node_ids, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR);
    void *d_rev_end_node_ids = gcl_malloc(graph->edge_count * sizeof(long) * NUM_BLOCKS, NULL, CL_MEM_READ_WRITE);
    
    
    /*
    for (int i = 0; i < graph->edge_count; i++ ) {
        printf("%d\n", graph->end_node_offsets[i]);
    }
     */
    
    
    
    dispatch_queue_t queue = gcl_create_dispatch_queue(CL_DEVICE_TYPE_CPU, NULL);
    cl_device_id gpu = gcl_get_device_id_with_dispatch_queue(queue);
    char name[128];
    clGetDeviceInfo(gpu, CL_DEVICE_NAME, 128, name, NULL);
    fprintf(stdout, "Created a dispatch queue using the %s\n", name);
    
    
    std::cout << "Graph copied to device" << std::endl;
    
    // create and copy shortest paths array
//    int *total_shortest_paths_a_node_is_on = (int *)malloc(graph->node_count * sizeof(int));
//    int *total_shortest_paths_buffer = (int *)malloc(graph->node_count * sizeof(int));

    long users_size = graph->node_count * sizeof(int) * work_group_size;
    printf("Users size: %lu\n", users_size);
//    exit(1);
    // is the number of shortest paths from s to t equivalent to the total number of shortest paths, eventually?
//    void *d_total_shortest_paths = gcl_malloc(graph->node_count * sizeof(int), NULL, CL_MEM_READ_WRITE);
    void *d_bfs_distances = gcl_malloc(users_size, NULL, CL_MEM_READ_WRITE);
    void *d_shortest_paths_to_other_nodes = gcl_malloc(users_size, NULL, CL_MEM_READ_WRITE);
    void *d_dependencies = gcl_malloc(graph->node_count * sizeof(float) * work_group_size, NULL,  CL_MEM_READ_WRITE);
    void *d_betweenness_centrality_measures = gcl_malloc(graph->node_count * sizeof(float) * work_group_size, NULL, CL_MEM_READ_WRITE);
    
    std::cout << "Dependency size: " << graph->node_count * work_group_size << std::endl;
    std::cout << "Betweenness centrality size: " << graph->node_count * work_group_size << std::endl;

    float *betweenness_centrality_measures_buffer = (float *)malloc(graph->node_count * sizeof(float) * work_group_size);
    
    for (int i = 0; i < graph->node_count * work_group_size; i++) {
        betweenness_centrality_measures_buffer[i] = 0.0;
    }
    
    float *betweenness_centrality_measures = (float *)malloc(graph->node_count * sizeof(float));
    
    for (int i = 0; i < graph->node_count; i++) {
        betweenness_centrality_measures[i] = 0.0;
    }


    for (size_t i = 0; i < graph->node_count / work_group_size; i++) {
//        std::cout << "Starting round " << i << std::endl;
        dispatch_sync(queue, ^{
//            std::cout << "In dispatch queue" << std::endl;
            size_t wgs;
            gcl_get_kernel_block_workgroup_info(
//                find_distance_for_node_kernel, CL_KERNEL_WORK_GROUP_SIZE, sizeof(wgs), &wgs, NULL
                find_distance_for_node_kernel, CL_KERNEL_WORK_GROUP_SIZE, sizeof(wgs), &wgs, NULL
            );
            
//            printf("CLKWGS: %d and WGS: %zu\n", CL_KERNEL_WORK_GROUP_SIZE, wgs);
            
//            size_t num_units = graph->node_count + (512 - graph->node_count % 512);
            /*
            cl_ndrange range = {
                1, {0, 0, 0},
                {num_units, 0, 0},
                {wgs, 0, 0}
            };
            */
            
            cl_ndrange range = {
                1,
                {i * work_group_size, 0, 0}, // offset
//                {1, 0, 0},
                {work_group_size, 0, 0},
//                {1, 0, 0},
                {1, 0, 0}
            };
            
            find_distance_for_node_kernel(
                 &range,
                 (cl_long)d_node_count,
                 (cl_long *)d_end_node_ids,
                 (cl_int *)d_end_node_offsets,
                 (cl_long *)d_rev_end_node_ids,
                 (cl_int *)d_rev_end_node_offsets,
                 (cl_int)d_edge_count,
                 (cl_int *)d_weights,
//                 (cl_int *)d_total_shortest_paths,
                 (cl_int *)d_bfs_distances,
                 (cl_int *)d_shortest_paths_to_other_nodes,
                 (cl_float *)d_dependencies,
                 (cl_float *)d_betweenness_centrality_measures
            );
            
//            gcl_memcpy(total_shortest_paths_buffer, d_total_shortest_paths, sizeof(int) * graph->node_count);
//            printf("Copying back to device\n");
            gcl_memcpy(betweenness_centrality_measures_buffer, d_betweenness_centrality_measures, users_size);
            
//            for (int i = 0; i < graph->node_count; i++) {
//                total_shortest_paths_a_node_is_on[i] += total_shortest_paths_buffer[i];
//            }
            for (int i = 0; i < work_group_size; i++) {
                for (int j = 0; j < graph->node_count; j++) {
                    betweenness_centrality_measures[j] += betweenness_centrality_measures_buffer[i * graph->node_count + j];
                }
            }
            
//            printf("Kernel finished\n");
//            exit(1);
        });
    }
    
    for (int i = 0; i < graph->node_count; i++) {
        // std::cout << "BC for node " << i << ": " << betweenness_centrality_measures[i] << std::endl;
//        printf("BC for node %d: %0.2f\n", i, betweenness_centrality_measures[i]);
    }
    
    // copy betweenness back to host
    
    /*
    std::cout << "Copying back to host" << std::endl;
    long total_shortest_paths = 0;
    for (int i = 0; i < graph->node_count; i++) {
        total_shortest_paths += total_shortest_paths_a_node_is_on[i];
    }
    std::cout << "Copy complete." << std::endl;
    for (int i = 0; i < graph->node_count; i++) {
        total_shortest_paths += total_shortest_paths_a_node_is_on[i];
        std::cout << "Node " << i << " has " << total_shortest_paths_a_node_is_on[i] << " shortest paths crossing through it" << std::endl;
    }
    */
    
    std::cout << "Writing to " << output_filename << std::endl;
    std::ofstream out_file (output_filename);
    double scaling_factor = 1.0 / (double)((graph->node_count - 1) * (graph->node_count - 2));
    if (out_file.is_open()) {
        out_file << "Node,Betweenness" << std::endl;
        for (int i = 0; i < graph->node_count; i++) {
            // TODO: fix as per http://stackoverflow.com/questions/11989374/floating-point-format-for-stdostream
//            out_file << i << "," << (float)total_shortest_paths_a_node_is_on[i] / total_shortest_paths << std::endl;
//            std::cout << betweenness_centrality_measures[i] << ": " << betweenness_centrality_measures[i] * scaling_factor << std::endl;
            out_file << i << "," << betweenness_centrality_measures[i] * scaling_factor << std::endl;
        }
        out_file.close();
    } else {
        std::cout << "Could not open out file" << std::endl;
    }
    
    // write betweenness to file
    
    std::cout << "Complete" << std::endl;
    
    return 0;
}
