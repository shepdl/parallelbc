import csv
import sys

in_filename, out_filename = sys.argv[1:3]
in_file = csv.DictReader(open(in_filename))
out_file = csv.DictWriter(open(out_filename, 'w'), ['source', 'target', 'weight',])

out_file.writeheader()

edges = {}
for row in in_file:
    edges.setdefault(row['user'], {})
    edges[row['user']].setdefault(row['mentioned_user'], 0)
    edges[row['user']][row['mentioned_user']] += 1

weights = {}
for user, mentioned_users in edges.items():
    for mentioned_user, weight in mentioned_users.items():
        out_file.writerow({
            'source' : user,
            'target' : mentioned_user,
            'weight' : weight
        })
        weights.setdefault(weight, 0)
        weights[weight] += 1

for weight, count in weights.items():
    print "{}: {}".format(weight, count)
